#include "precompiled.h"


#include "soundtrack.h"
#include "Game_Snake.h"

DWORD __stdcall gameThreadFunction(void* subject);
long __stdcall hook_proc(int code, unsigned int wParam, long lParam);
DWORD __stdcall keyboardMessageLoop();

HHOOK KeyboardHook;
theGame *the_game;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	setlocale(LC_ALL, "Russian");

	the_game = new Game_Snake(2,GetCurrentThreadId());
	LPDWORD threadID = nullptr;
	the_game->Launch();
	void* gameThread = CreateThread(NULL,0,&gameThreadFunction, the_game, 0,threadID);
	HANDLE input= GetStdHandle(STD_INPUT_HANDLE);
	INPUT_RECORD ir;
	DWORD nEventsRead=0;

	if (KeyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, hook_proc, NULL, NULL))
		keyboardMessageLoop();

	delete the_game;

	return 0;// keyboardMessageLoop(nullptr);
}
long __stdcall hook_proc(int code, unsigned int wParam, long lParam)
{
	#pragma region hide
	//MAY CAUSE SOME TROUBLES, IDK HOW IT'LL BEHAVE
	//if (code<0)
		//return CallNextHookEx(KeyboardHook, code, wParam, lParam);//marshalls current message to another processes that hooked to the same resource
	//if WH_KEYBOARD_LL is used instead of WH_KEYBOARD, wParam contains some bullshit about type of message : the identifier of the keyboard message (wm_keydown/up etc)

	//keydown = 100h 
	//keyup = 101h

	//������. ������� �������� WM_KEYDOWN. ��� ������������
	//����� ��� �� ��������� WM_KEYUP, ��� ���� �����.
	//MessageBox(NULL, to_wstring(wParam).c_str(), L"wParam", MB_OK);
	
	//if WH_KEYBOARD_LL is used instead of WH_KEYBOARD, then lParam is a pointer to 
	//KBDLLHOOKSTRUCT* KbdHookStruct = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);
	//return 0;
#pragma endregion

	if (wParam == WM_KEYDOWN)
	{
		KBDLLHOOKSTRUCT *KbdInfo = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);

		switch (KbdInfo->vkCode)
		{
		case VK_UP:
		case VK_RIGHT:
		case VK_DOWN:
		case VK_LEFT:
			if (the_game->OnAir())
				the_game->Input_SetDirection(static_cast<Direction>(KbdInfo->vkCode));
			break; 
		case VK_SPACE:
			the_game->TogglePause();
			
			break;
		case VK_ESCAPE:
			the_game->Exit();
			break;
		default:
			//MessageBox(NULL, L"key unknown is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		}
	}

	return CallNextHookEx(KeyboardHook, code, wParam, lParam);
}


//AM NOT EVEN SURE IF I NEED THIS
DWORD __stdcall keyboardMessageLoop()
   {
	MSG msg;
	BOOL bRet;
	//MessageBox(NULL, L"IN HERE", L"dad", MB_OK);
	while ((bRet= GetMessage(&msg, NULL, 0, 0)) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

	}
	UnhookWindowsHookEx(KeyboardHook);
	return msg.wParam;
}

DWORD __stdcall gameThreadFunction(void * subject)
{
	Game_Snake *game = static_cast<Game_Snake*>(subject);
	game->hThread_GameLoop = GetCurrentThread();
	while (game->OnAir())
		game->GameLoop();
	return 0;
}
