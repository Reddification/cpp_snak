
#ifndef GAME_SNAKE
#define GAME_SNAKE

#include "theGame.h"
#include "Snake.h"

class Game_Snake:public theGame {
public:
	void Launch() override;
	void GameLoop() override;
	void Exit();
	void TogglePause();
	void Input_SetDirection(Direction);// override;

	Snake* const get_snake();
	Game_Snake(int, DWORD);
	//they say destructors are called in inverse order of inheretance (from childs to parents)
	~Game_Snake();

	//USELESS, educational purposes only
	friend void keyboardEventHandler(Game_Snake*, WPARAM, LPARAM);
protected:
	void Gameover() override;
	//static LRESULT WINAPI KeyboardEventHandler(int, unsigned int, long);
private:
	short mapWidth, mapHeight;
	Snake *snak;
	Food* _food;

	void* hThread_DropFood;
	void* hEvent_DropFood;

	void ClearGameField();
	void DrawSnake();
	void DrawMap();
	void DrawFood();
	bool CanBeEaten();

	bool isOverlappingMap();
	static DWORD __stdcall StartSoundtrack(void* lpParam);
	static DWORD WINAPI DropFood(void* lpParam);
	//void DropFood();
	//void Render();
};

#endif // !GAME_SNAKE