#include "Misc.h"

#ifndef THEGAME
#define THEGAME

class theGame {
public:
	virtual void Launch()=0;
	virtual	void GameLoop()=0;
	//const void* get_handleCSB(int n);
	virtual void Exit() = 0;
	virtual void TogglePause() = 0;
	virtual void Input_SetDirection(Direction)=0;

	const bool &OnAir() const;
	const int& get_score();

	theGame(int, DWORD);
	virtual ~theGame();

protected:
	//virtual void KeyboardEventHandler() = 0;
	virtual void Gameover() = 0;
	void SetConsoleWindow();
	void SwapCSB();
	
	void update_score(UINT plus) const;

	class Soundtrack *OST;

	void* hThread_Soundtrack;
	void* hThread_Render;
	//void* hThread_GameLogic;
	void* hThread_GameLoop;

	void* hMutex_Render;
	void* hMutex_Soundtrack;
	//void* hMutex_GameLogic;
	void* hMutex_GameLoop;

	unsigned long threadID;
	unsigned int _latency;
	unsigned int vLatency, hLatency;
	bool onAir;
	bool _paused;
	mutable UINT score;
	//HHOOK KeyboardHook;

	int _currentScreenBufferNumber;
	void **hConsoleScreenBuffers;
	int _amountOfScreenBuffers;
	DWORD mainThreadID;//�� ����
	//void* hConsoleScreenBuffer_1;
	//void* hConsoleScreenBuffer_2;
	//byte currentCSB;
private:
	void SyncWindows();
	friend DWORD WINAPI gameThreadFunction(void * subject);
	//void DropFood();
};

#endif // !THEGAME

