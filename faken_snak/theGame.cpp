﻿#include "precompiled.h"
#include "theGame.h"
#include <string>



void theGame::SetConsoleWindow()
{
	AllocConsole();

	HWND console = GetConsoleWindow();
	RECT ConsoleRect;
	GetWindowRect(console, &ConsoleRect);
	MoveWindow(console, ConsoleRect.left, ConsoleRect.top-25, 500, 350, TRUE);

	//freopen("CONIN$", "r", stdin); ← THIS enables cins
	HANDLE consoleOutputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	//void* consoleInputHandle = GetStdHandle(STD_INPUT_HANDLE);
	SetConsoleTitle(TEXT("FAKKEN SNAEK"));
	SyncWindows();
	
	//this one is supposed to get active screen buffer, so no need to create 2 more, 1 is enough
	//void* uselessHandle = CreateFile(L"CONOUT$", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);// this one is fucking useless

	CONSOLE_CURSOR_INFO cci;
	//CONSOLE_FONT_INFOEX cfie;
	cci.bVisible = false;
	cci.dwSize = 1;
	//COORD size; size.X = 120; size.Y = 40;

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	for (auto i = 0; i < _amountOfScreenBuffers; i++)
	{
		this->hConsoleScreenBuffers[i] = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
		SetConsoleTextAttribute(hConsoleScreenBuffers[i], 0x0a);
		SetConsoleCursorInfo(hConsoleScreenBuffers[i], &cci);
		#pragma region hide

		//GetCurrentConsoleFontEx(this->hConsoleScreenBuffers[i], false, &cfie);
		//cfie.dwFontSize.X = cfie.dwFontSize.Y = 4;
		//SetCurrentConsoleFontEx(hConsoleScreenBuffers[i], true, &cfie);
		//GetConsoleScreenBufferInfo(this->hConsoleScreenBuffers[i], &csbi);

#pragma endregion
	}

	SetConsoleActiveScreenBuffer(hConsoleScreenBuffers[_currentScreenBufferNumber]);
	CloseHandle(consoleOutputHandle);
}
void theGame::SwapCSB()
{
	this->_currentScreenBufferNumber = (_currentScreenBufferNumber + 1) % _amountOfScreenBuffers;
		SetConsoleActiveScreenBuffer(this->hConsoleScreenBuffers[_currentScreenBufferNumber]);
	#pragma region hide
	/*if (!this->currentCSB)
		SetConsoleActiveScreenBuffer(this->hConsoleScreenBuffer_2);
	else SetConsoleActiveScreenBuffer(this->hConsoleScreenBuffer_1);
	this->currentCSB ^= 1;
*/
	//freopen("CONOUT$", "w", stdout);

#pragma endregion
}
void theGame::update_score(UINT plus) const
{
	this->score += plus;
}

void theGame::SyncWindows()
{
	HWND consoleWindow = GetConsoleWindow();
	LONG style = GetWindowLong(consoleWindow, GWL_STYLE);
	style = style & ~(WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_HSCROLL|WS_VSCROLL|WS_SIZEBOX);
	SetWindowLong(consoleWindow, GWL_STYLE, style);
}

theGame::~theGame()
{

	for (auto i = 0; i < _amountOfScreenBuffers; i++)
		CloseHandle(this->hConsoleScreenBuffers[i]);

	delete[] this->hConsoleScreenBuffers;

	//CloseHandle(this->hThread_Soundtrack);
	CloseHandle(this->hThread_GameLoop);

	//CloseHandle(this->hMutex_Soundtrack);
	CloseHandle(this->hMutex_GameLoop);
	
	/*try {FreeConsole();}
	catch (std::exception e)
	{
		MessageBoxA(0, e.what(), "EXCEP", MB_OK);
	}*/
}

const  bool &theGame::OnAir() const
{
	return this->onAir;
}

const int & theGame::get_score()
{
	return this->score;
}

//const void * theGame::get_handleCSB(int n)
//{
//	return this->hConsoleScreenBuffers[n];
//}

theGame::theGame(int n, DWORD _mainThreadID)
{
	this->hConsoleScreenBuffers = new void*[n];
	this->_currentScreenBufferNumber = 0;
	this->_amountOfScreenBuffers = n;
	this->mainThreadID = _mainThreadID;
}

