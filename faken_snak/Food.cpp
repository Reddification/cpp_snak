#include "precompiled.h"
#include "Food.h"

#ifndef __FOODO__
#define __FOOOD__
Food::Food(coordinate &c, WORD w, void * _hEvent)
{
	this->location = c;
	this->weight = w;
	this->hEvent = _hEvent;
}

Food::~Food()
{
	SetEvent(this->hEvent);
}

const coordinate & Food::get_location()
{
	return this->location;
}

const WORD & Food::get_weight()
{
	return this->weight;
}

//inline Food::operator const COORD()
//{
//	COORD c;
//	c.X = location.X;
//	c.Y = location.Y;
//	return c;
//}

#endif // !__FOODO__


