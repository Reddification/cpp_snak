#include "precompiled.h"

#ifndef __DIRECTION__D_
#define __DIRECTION__D_
enum class Direction:WORD { 
	UP=VK_UP, DOWN=VK_DOWN, LEFT=VK_LEFT, RIGHT=VK_RIGHT
};

using T = std::underlying_type_t <Direction>;
inline Direction operator | (Direction a, Direction b)
{
	return static_cast<Direction>(static_cast<T>(a)|static_cast<T>(b));
}
inline Direction& operator |= (Direction& lhs, Direction rhs)
{
	lhs = (Direction)(static_cast<T>(lhs) | static_cast<T>(rhs));
	return lhs;
}

inline Direction operator & (Direction a, Direction b)
{
	return static_cast<Direction>(static_cast<T>(a)&static_cast<T>(b));
}
inline Direction& operator &= (Direction &a, Direction b)
{
	a = static_cast<Direction>(static_cast<T>(a)&static_cast<T>(b));
	return a;
}

inline bool operator > (Direction a, T n) 
{return static_cast<T>(a) > n;}
inline bool operator < (Direction a, T n)
{
	return static_cast<T>(a) < n;
}
inline bool operator == (Direction a, T n)
{
	return static_cast<T>(a) == n;
}

struct coordinate {
	short X, Y;
	coordinate(short x, short y)
	{
		this->X = x;
		this->Y = y;
	}
	coordinate()
	{
		X = Y = 0;
	}
	inline operator COORD ()
	{
		COORD c;
		c.X = X;
		c.Y = Y;
		return c;
	}
};
#endif // !__DIRECTION__D_

