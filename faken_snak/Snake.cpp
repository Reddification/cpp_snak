#include "precompiled.h"
#include "Snake.h"


std::vector<coordinate> operator += (std::vector<coordinate> &v,coordinate d)
{
	for (coordinate &c : v)
	{
		c.X += d.X;
		c.Y += d.Y;
	}
	return v;
}


void Snake::Eat(Food* _food)
{
	WORD w = _food->get_weight();
	this->length += w;
	this->_undigested += w;
	delete _food;
	//ReleaseMutex(hMutex_DropFood);
}

bool Snake::Move()
{
	coordinate moveDirection;
	switch (currentDirection)
	{
	case Direction::UP:
		moveDirection.X = 0; moveDirection.Y = -1;
		break;
	case Direction::DOWN:
		moveDirection.X = 0; moveDirection.Y = 1;
		break;
	case Direction::LEFT:
		moveDirection.X = -1; moveDirection.Y = 0;
		break;
	case Direction::RIGHT:
		moveDirection.X = 1; moveDirection.Y = 0;
		break;
	default:
		break;
	}

	//this->_coordinates += moveDirection;

	const coordinate *head = this->Head();
	this->_coordinates.insert(_coordinates.begin(),coordinate(head->X+moveDirection.X, head->Y+moveDirection.Y));
	if (_undigested == 0)
		_coordinates.pop_back();
	else _undigested--;

	movePending = false;
	return !isOverlapping();
}

Snake::~Snake()
{
	(std::vector<coordinate>()).swap(this->_coordinates);
}

Snake::Snake(coordinate origins)
{
	this->length = 2;
	movePending = false;
	this->set_direction(Direction::LEFT);
	this->_coordinates.push_back(origins);
	origins.X++;
	this->_coordinates.push_back(origins);
	this->_undigested = 0;
}

//Snake* Snake::operator+=(Food *f)
//{
//	WORD w = f->get_weight();
//		//std::vector<coordinate>::iterator tail = this->_coordinates.end();
//	this->length += w;//f->get_weight();
//	this->_undigested += w;
//
//	delete f;
//	return this;
//}

//Snake *Snake::operator+(Food &f)
//{
//	this->length += f.get_weight();
//	this->_undigested += f.get_weight();
//	delete &f;
//	return this;
//}

const Direction & Snake::get_direction()
{
	return this->currentDirection;
}

void Snake::set_direction(Direction d)
{
	if (movePending)
		return;

	switch (d)
	{
	case Direction::UP:
		if (this->currentDirection != Direction::DOWN&&this->currentDirection != d)
		{
			this->currentDirection = d;
			this->head = 'A';
			movePending = true;
		}
		break;
	case Direction::DOWN:
		if (this->currentDirection != Direction::UP&&this->currentDirection != d)
		{
			this->currentDirection = d;
			this->head = 'V';
			movePending = true;
		}
		break;
	case Direction::LEFT:
		if (this->currentDirection != Direction::RIGHT&&this->currentDirection != d)
		{
			this->currentDirection = d;
			this->head = '<';
			movePending = true;
		}
		break;
	case Direction::RIGHT:
		if (this->currentDirection != Direction::LEFT&&this->currentDirection != d)
		{
			this->currentDirection = d;
			this->head = '>';
			movePending = true;
		}
		break;
	default:
		break;
	}
}

const coordinate * Snake::Head()
{
	return &this->_coordinates[0];
}

bool Snake::isOverlapping()
{
	for (auto i = 1; i < _coordinates.size(); i++)
		if (_coordinates[0].X == _coordinates[i].X&&_coordinates[0].Y == _coordinates[i].Y)
			return true;
	return false;
}
