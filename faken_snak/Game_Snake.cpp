 #include "precompiled.h"
#include "Game_Snake.h"
#include "soundtrack.h"
//#include "Food.h"
//#include "Aclapi.h"

//DWORD (WINAPI Game_Snake::*soundtrackDelegate)(LPSECURITY_ATTRIBUTES, SIZE_T, LPTHREAD_START_ROUTINE, void*, LPDWORD) = NULL;
//LRESULT (WINAPI Game_Snake::*keyboardHookDelegate)(int, unsigned int, long) = NULL;

using namespace std;


//executed on main thread
void Game_Snake::Launch()
{
	//keyboardHookDelegate = &Game_Snake::KeyboardEventHandler;
	
	#pragma region console window and map
	this->SetConsoleWindow();
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(*hConsoleScreenBuffers,&csbi);
	this->mapWidth = csbi.dwSize.X;
	this->mapHeight = csbi.dwSize.Y-2;
	this->DrawMap();
	//SwapCSB();
	//this->DrawMap();
#pragma endregion

	this->snak = new Snake(coordinate(csbi.dwSize.X/2,csbi.dwSize.Y/2-1));

	//DWORD (*threadFunction)(void* lpParam) = (StartSoundtrack);
	
	#pragma region threads and mutexes
	this->hMutex_GameLoop = CreateMutex(NULL, false, L"gameloopMutex");
	
	this->hEvent_DropFood = CreateEvent(NULL,false,false, L"dropfoodEvent");
	this->hThread_DropFood = CreateThread(NULL, 0, &Game_Snake::DropFood, this, 0, 0);
#pragma endregion

	//SetSecurityInfo(hSoundtrack_thread, SE_OBJECT_TYPE::SE_KERNEL_OBJECT, THREAD_SUSPEND_RESUME, 0, 0, 0, 0);
	//DWORD x = GetSecurityInfo(hSoundtrack_thread, SE_OBJECT_TYPE::SE_KERNEL_OBJECT, 0, 0, 0, 0, 0, 0	);
	#pragma region variables

	this->onAir = true;
	this->_paused = false;
	this->hLatency = 100;
	this->vLatency = 100;
	this->score = 0;
	this->_latency = hLatency;
	SetThreadPriority(this->hThread_Soundtrack, THREAD_PRIORITY_BELOW_NORMAL);

	srand(static_cast<unsigned int>(time(NULL)));
#pragma endregion

}


DWORD WINAPI Game_Snake::StartSoundtrack(void* lpParam)
{
	Soundtrack *OST = new Soundtrack();
	//Game_Snake *subject = static_cast<Game_Snake*>(lpParam);

	//MessageBox(NULL, to_wstring(GetCurrentThreadId()).c_str(), L"StartSoundtrack", MB_OK);

	OST->Play(true);
	ExitThread(0);
}


//TODO
DWORD WINAPI Game_Snake::DropFood(void * lpParam)
{
	Game_Snake *subject = static_cast<Game_Snake*>(lpParam);

	void* hEvent = OpenEvent(EVENT_ALL_ACCESS, false, L"dropfoodEvent");

	while (subject->OnAir())
	{
		subject->_food = new Food(coordinate(1+rand()%(subject->mapWidth-2), 2+rand()%(subject->mapHeight-3)),1, subject->hEvent_DropFood);
		WaitForSingleObject(hEvent, INFINITE);//NYET, �� ��� �������� �������� ��� ����
	}
	return 0;
}


//working
void Game_Snake::GameLoop()
{
	WaitForSingleObject(this->hMutex_GameLoop,INFINITE);
	bool isFine = this->snak->Move() && !isOverlappingMap();
	ClearGameField();
	if (this->_food)
		DrawFood();
	DrawSnake();
	if (isFine&&this->_food&&CanBeEaten())
	{
		WORD w = _food->get_weight();
		this->score += w*this->_latency;
		snak->Eat(_food);
	}
	this->SwapCSB();
	if (!isFine)
		Gameover();
	
	ReleaseMutex(this->hMutex_GameLoop);
	Sleep(this->_latency);
}

void Game_Snake::Exit()
{
	this->TogglePause();
	Gameover();
}

void Game_Snake::TogglePause()
{
	_paused = !_paused;
	if (!_paused)
	{
		//WaitForSingleObject(this->hMutex_Soundtrack, INFINITE);
		WaitForSingleObject(this->hMutex_GameLoop, INFINITE);
	}
	else
	{
		//ReleaseMutex(hMutex_Soundtrack);
		ReleaseMutex(this->hMutex_GameLoop);
	}

		//ResumeThread(hThread_Soundtrack);
		//SuspendThread(hThread_Soundtrack);
}

void Game_Snake::Input_SetDirection(Direction d)
{
	this->snak->set_direction(d);

	if ((d&(Direction::DOWN | Direction::UP)) > 0 && this->_latency != vLatency)
		this->_latency = vLatency;
	else if ((d&(Direction::RIGHT | Direction::LEFT)) > 0 && this->_latency != hLatency)
		this->_latency = hLatency;
}

void Game_Snake::DrawSnake()
{
	void* hBuffer = this->hConsoleScreenBuffers[(_currentScreenBufferNumber + 1) % _amountOfScreenBuffers];
	//DuplicateHandle(hProcess, hBuffer, hProcess,&hBuffer,GENERIC_READ | GENERIC_WRITE,false,DUPLICATE_SAME_ACCESS);
	COORD _coord = this->snak->_coordinates[0];

	for (auto i=this->snak->length-1;i>0;i--)
	{
		_coord = (COORD)this->snak->_coordinates[i];
		SetConsoleCursorPosition(hBuffer, _coord);
		WriteConsole(hBuffer, &this->snak->body, 1, NULL, NULL);
	}
	_coord = this->snak->_coordinates[0];
	SetConsoleCursorPosition(hBuffer, _coord);
	WriteConsole(hBuffer, &this->snak->head, 1, NULL, NULL);
}

void Game_Snake::DrawMap()
{
	COORD newPosition; newPosition.X = newPosition.Y = 0;
	void* currentBuffer;
	for (auto i = 0; i < _amountOfScreenBuffers; i++)
	{
		currentBuffer = this->hConsoleScreenBuffers[i];
		newPosition.X = newPosition.Y = 0;
		SetConsoleCursorPosition(currentBuffer, newPosition);
		WriteConsole(currentBuffer, L"\tlab4: the snake", 16, NULL, NULL);

		newPosition.X = 0; newPosition.Y = 1;
		SetConsoleCursorPosition(currentBuffer, newPosition);
		//cout << "lab4: SNAKE game" << endl;
		for (auto y = 0; y < mapHeight - 1; y++)
		{
			if (y == 0 || y == mapHeight - 2)
				for (auto x = 0; x < mapWidth; x++)
					WriteConsole(currentBuffer, L"#", 1, NULL, NULL);
			//cout << setfill('#')<<setw(mapWidth-1)<<endl;
			else
			{
				WriteConsole(currentBuffer, L"#", 1, NULL, NULL);
				newPosition.X = mapWidth - 1;
				newPosition.Y = y + 1;
				SetConsoleCursorPosition(currentBuffer, newPosition);
				WriteConsole(currentBuffer, L"#", 1, NULL, NULL);
			}
			newPosition.X = 0; newPosition.Y += 1;
			SetConsoleCursorPosition(currentBuffer, newPosition);
		}
	}
			//cout << "#";
			//cout << setfill(' ') << setw(mapWidth -3) << '#' << endl;
}

void Game_Snake::DrawFood()
{
	COORD c; 
	c.X= this->_food->get_location().X;
	c.Y = this->_food->get_location().Y;
	void* hBuffer = this->hConsoleScreenBuffers[(_currentScreenBufferNumber + 1) % _amountOfScreenBuffers];
	SetConsoleCursorPosition(hBuffer, c);
	SetConsoleTextAttribute(hBuffer, 0x09);
	WriteConsole(hBuffer, &_food->Sign, 1, NULL, NULL);
	SetConsoleTextAttribute(hBuffer, 0x0A);

}

bool Game_Snake::CanBeEaten()
{
	return snak->Head()->X == _food->get_location().X&&snak->Head()->Y == _food->get_location().Y;
}

void Game_Snake::ClearGameField()
{
	void* hBuffer = this->hConsoleScreenBuffers[(_currentScreenBufferNumber + 1) % _amountOfScreenBuffers];
	//SwapCSB();
	COORD cBegin; 
	cBegin.X = 1;
	DWORD fuckyou = 0;
	for (auto i = 2; i < this->mapHeight-1; i++)
	{
		cBegin.Y = i;
		FillConsoleOutputCharacterA(hBuffer, ' ', mapWidth - 2, cBegin, &fuckyou);
	}
	cBegin.X = 0;
	cBegin.Y = mapHeight;
	FillConsoleOutputCharacterW(hBuffer, ' ', mapWidth, cBegin, &fuckyou);
	wstring statistics = L"����� ����: " +to_wstring(this->snak->length) + L"; ������� ����: " + to_wstring(this->score);
	SetConsoleCursorPosition(hBuffer, cBegin);
	WriteConsole(hBuffer, statistics.c_str(), statistics.length(), NULL, NULL);
	//SwapCSB();
}

bool Game_Snake::isOverlappingMap()
{
	const coordinate *snakeHead = this->snak->Head();
	return snakeHead->X == 0 || snakeHead->X == mapWidth-1 || snakeHead->Y == 1 || snakeHead->Y == mapHeight-1;
}

Snake * const Game_Snake::get_snake()
{
	return this->snak;
}

Game_Snake::Game_Snake(int n, DWORD _mainThreadID) : theGame(n, _mainThreadID)
{ }

Game_Snake::~Game_Snake()
{
	CloseHandle(this->hEvent_DropFood);
	CloseHandle(this->hThread_DropFood);

	delete this->snak;
	if (this->_food)
		delete this->_food;
}

void Game_Snake::Gameover()
{
	wstring rezult = L"����: " + to_wstring(score) + L"\n����� ����: " +to_wstring(this->snak->length);
	MessageBox(NULL, rezult.c_str(), L"����� ����", MB_OK);
	this->onAir = false;
 	PostThreadMessageW(this->mainThreadID,WM_QUIT,0,0);
}


//actually, useless. just a useless example of using friend functions
void keyboardEventHandler(Game_Snake* the_game, WPARAM wParam, LPARAM lParam)
{
	if (wParam == WM_KEYDOWN)
	{
		KBDLLHOOKSTRUCT *KbdInfo = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);

		switch (KbdInfo->vkCode)
		{
		case VK_UP:
			the_game->Input_SetDirection(Direction::UP);
			//MessageBox(NULL, L"key up is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		case VK_RIGHT:
			the_game->Input_SetDirection(Direction::RIGHT);
			//MessageBox(NULL, L"key right is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		case VK_DOWN:
			the_game->Input_SetDirection(Direction::DOWN);
			//MessageBox(NULL, L"key down is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		case VK_LEFT:
			the_game->Input_SetDirection(Direction::LEFT);
			//MessageBox(NULL, L"key left is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		case VK_SPACE:
			the_game->TogglePause();
			//MessageBox(NULL, L"key space is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		case VK_ESCAPE:
			the_game->Exit();
			//MessageBox(NULL, L"key escape is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		default:
			//MessageBox(NULL, L"key unknown is pressed", L"KEYBOARD HOOK WORKED", MB_OK);
			break;
		}
	}
}
